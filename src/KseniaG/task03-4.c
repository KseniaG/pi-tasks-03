#include <stdio.h>
#include <time.h>
#include<stdlib.h>
#define N 15
int i, arr[N];
int min, max;
float med = 0;
int min_max_med(int *arr)
{
	min = arr[0];
	max = arr[0];
	for (i = 0; i < N; i++)
	{
		if (arr[i] < min)
			min = arr[i];
		if (arr[i] > max)
			max = arr[i];
		med += arr[i];
	}
}
int main()
{
	srand(time(0));
	for (i = 0; i < N; i++)
	{
		arr[i] = rand() % 100;
		printf("%d \n", arr[i]);
	}
	min_max_med(arr);
	printf("Min = %d, Max = %d, Medium = %.3f\n", min, max, med / N);
	return 0;
}