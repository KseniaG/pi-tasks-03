#include <stdio.h>
#include <string.h>
int main()
{
	char str[256];
	int i = 0, length, arr[256];
	puts("Enter a string \n");
	fgets(str, 256, stdin);
	for (i = 0; i < 256; i++)
		arr[i] = 0;
	for (i = 0; i < strlen(str) - 1; i++)
		arr[str[i]]++;
	for (i = 0; i < 256; i++)
		if (arr[i] != 0)
		{
			length = i;
			printf("%c-%d,", length, arr[i]);
		}
	printf("\b \n");
	return 0;
}